package com.realdolmen;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FractionTest {
    @Test
    @DisplayName("Check that the Numerator and Denominator are correct.")
    public void assertThatNumeratorAndDenominatorAreCorrect(){
        Fraction fraction = new Fraction(1,2);
        Assertions.assertEquals(1, fraction.getNumerator());
        Assertions.assertEquals(2, fraction.getDenominator());
    }

    @Test
    public void checkThatToStringWorks(){
        Fraction fraction = new Fraction(1,2);
        Assertions.assertEquals("1/2", fraction.toString());
    }

    @Test
    public void checkThatSimplifyWorks(){
        Fraction fraction = new Fraction(2,4);
        Assertions.assertEquals("1/2", fraction.toString());
    }

    @Test
    public void givenFraction_whenMultiplyingTwoFractions_thenReturnsMultiplication(){
        Fraction a = new Fraction(3,4);
        Fraction b = new Fraction(2,4);
        Assertions.assertEquals("3/8",a.multiply(b).toString());
    }

    @Test
    public void givenFraction_whenAddingTwoFractions_thenReturnsSum(){
        Fraction a = new Fraction(6,8);
        Fraction b = new Fraction(2,4);
        Assertions.assertEquals("5/4",a.add(b).toString());
    }

    @Test
    public void givenFraction_whenSubtractingTwoFractions_thenReturnsSubtraction(){
        Fraction a = new Fraction(6,8);
        Fraction b = new Fraction(2,4);
        Assertions.assertEquals("1/4",a.subtract(b).toString());
    }

    @Test
    public void givenFraction_whenDividingTwoFractions_thenReturnsDivision(){
        Fraction a = new Fraction(6,8);
        Fraction b = new Fraction(2,4);
        Assertions.assertEquals("3/2",a.divide(b).toString());
    }

}
