package com.realdolmen;

public class Fraction {
    private int numerator;
    private int denominator;

    public Fraction(int numerator, int denominator){
        this.numerator = numerator;
        this.denominator = denominator;
        simplify();
    }
    public String toString(){
        return numerator + "/" + denominator;
    }

    private void simplify(){
        int greatestCommonFactor = Utilities.greatestCommonFactor(numerator,denominator);
        this.numerator /= greatestCommonFactor;
        this.denominator /= greatestCommonFactor;
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public Fraction multiply(Fraction b){
        return new Fraction(this.numerator* b.numerator, this.denominator* b.denominator);
    }

    public Fraction add(Fraction b){
        return new Fraction(this.numerator* b.denominator+this.denominator*b.numerator, this.denominator* b.denominator);
    }

    public Fraction subtract(Fraction b){
        return new Fraction(this.numerator* b.denominator-this.denominator*b.numerator, this.denominator* b.denominator);
    }

    public Fraction divide(Fraction b){
        return new Fraction(this.numerator* b.denominator, this.denominator* b.numerator);
    }
}
